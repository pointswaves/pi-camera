# Big yellow camera

The big yellow camera is is a basic example of a open source pi camera

![A front view of the camera](./pics/camera_front.jpg "Camera")

It can generate ok images in a verity of light conditions.

![A example photo of William in dark environment](./pics/sample_photo.png "Example")

The images are mostly held back by the software rather than the hardware and patches are welcome to improve things

The hardware is described in the [hardware readme](./hardware/readme.md)

## Building the Full image

The easiest way to build a sd card image for the camera is to use the girdersteram project in the `integration` folder.

Note: When running girdersteram you need casd running in a separate terminal.

```bash
buildbox-casd --protect-session-blobs --bind localhost:50040 ~/.cache/girderstream/cas
```

Then you can build the pi sd card image:

```bash
cd integration
girderstream build --name filesystem-target
```

Once the filesystem is built we can check it out

```bash
girderstream checkout --name filesystem-target --location ./filesystem_pi
```

## Burn the image to the sd card

First check the name of your sd card.
```bash
✦ ❯ lsblk 
NAME                                          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
[..]
sdb                                             8:16   1  29.7G  0 disk  
├─sdb1                                          8:17   1   300M  0 part  
└─sdb2                                          8:18   1     2G  0 part
[..]
```

For the pi you should write to a sd card like:

Note: using sudo and dd can result in over riding very important files and making you have a very bad day, if you dont know what your doing please use something like etcher or the tools provided by the Pi foundation.

```bash
sudo dd bs=64k status=progress of=/dev/sdX if=filesystem_pi/disk.img
sync
```


## Developing the app

The girderstream test project generates a sdk to help with local development. This sdk can be used in many ways, please see there docs for details.

The quickest and easiest may depend on your us case but for those used to docker the following can be used.

Launch the sdk via docker, podman running in user mode is less likely to cause permissions issues so it is advised to use it but the cli is exactly the same.

```bash
podman run -it -v `pwd`:/src/:z --workdir /src/ registry.gitlab.com/pointswaves/girderstream-test-project/sdk-aarch64:latest bash
```

```bash
docker run -it -v `pwd`:/src/:z --workdir /src/ registry.gitlab.com/pointswaves/girderstream-test-project/sdk-aarch64:latest bash
```

Then from inside the new terminal you can build the app for the pi.

```bash
cargo build --target=aarch64-unknown-linux-gnu
```

This app is compiled for the pi and so is unlikely to work on your local machine.

To move the app to a sd you would map you sd card to `/run/media/${USER}/ROOT/`, typically nautilus or files will do this for you. Then you move the bin and service file to the sd card.

```bash
sudo cp target/aarch64-unknown-linux-gnu/debug/picamera /run/media/${USER}/ROOT/usr/bin/
sudo cp cam.service /run/media/will/ROOT/usr/lib/systemd/system
sudo mkdir -p /run/media/will/ROOT/root/images/
sudo sync
```

