/*
# SPI ST7789 on a Raspberry Pi Zero W Example


*/

use std::{
    process::{Command, ExitCode},
    thread,
    time::{self, Duration},
};

use display_interface_spi::SPIInterfaceNoCS;
use embedded_graphics::{
    image::{Image, ImageRaw, ImageRawBE, ImageRawLE},
    mono_font::{ascii::FONT_10X20, MonoTextStyle},
    pixelcolor::{Rgb565, Rgb888},
    prelude::*,
    text::Text,
};
use image;
use libcamera::{
    camera::CameraConfigurationStatus,
    camera_manager::CameraManager,
    control::Property,
    framebuffer::AsFrameBuffer,
    framebuffer_allocator::{FrameBuffer, FrameBufferAllocator},
    framebuffer_map::MemoryMappedFrameBuffer,
    pixel_format::PixelFormat,
    properties,
    request::ReuseFlag,
    stream::StreamRole,
};
use mipidsi::{Builder, ModelOptions};
use rand::Rng;
use rppal::gpio::{Gpio, OutputPin};
use rppal::hal::Delay;
use rppal::spi::{Bus, Mode, SlaveSelect, Spi};

// Pins

const SPI_DC: u8 = 25;
const BACKLIGHT: u8 = 26;

// Display
const W: i32 = 240;
const H: i32 = 240;

const PIXEL_FORMAT_RGB565: PixelFormat = PixelFormat::new(909199186, 0);
const PIXEL_FORMAT_RGB888: PixelFormat = PixelFormat::new(875710274, 0);

fn main() -> ExitCode {
    println!("Hi there");
    let mut rng = rand::thread_rng();

    let five_sec = time::Duration::from_secs(5);
    thread::sleep(five_sec);

    let mgr = CameraManager::new().unwrap();

    let cameras = mgr.cameras();

    let cam = cameras.get(0).expect("No cameras found");

    println!(
        "Using camera: {}",
        *cam.properties().get::<properties::Model>().unwrap()
    );

    let mut cam = cam.acquire().expect("Unable to acquire camera");

    println!("Properties: {:#?}", cam.properties());

    let mut config = cam
        .generate_configuration(&[StreamRole::ViewFinder])
        .unwrap();
    let view_finder_cfg = config.get(0).unwrap();
    //println!("Available formats: {:#?}", view_finder_cfg.formats());
    for pf in view_finder_cfg.formats().pixel_formats().into_iter() {
        println!("pf: {:#?}", pf);
        println!(" fourcc: {:#?}", pf.fourcc());
        println!(" mod: {:#?}", pf.modifier());
    }

    println!("{config:?}");
    config
        .get_mut(0)
        .unwrap()
        .set_pixel_format(PIXEL_FORMAT_RGB888);

    //config.get_mut(0).unwrap().set_frame_size(frame_size)

    println!("Generated config: {:#?}", config);

    match config.validate() {
        CameraConfigurationStatus::Valid => println!("Camera configuration valid!"),
        CameraConfigurationStatus::Adjusted => {
            println!("Camera configuration was adjusted: {:#?}", config)
        }
        CameraConfigurationStatus::Invalid => panic!("Error validating camera configuration"),
    }

    // Ensure that pixel format was unchanged
    if 1 == 1 {
        assert_eq!(
            config.get(0).unwrap().get_pixel_format(),
            PIXEL_FORMAT_RGB888,
            "NV12 is not supported by the camera"
        );
    };

    cam.configure(&mut config)
        .expect("Unable to configure camera");

    let mut alloc = FrameBufferAllocator::new(&cam);

    // Allocate frame buffers for the stream
    let cfg = config.get(0).unwrap();
    let stream = cfg.stream().unwrap();
    let buffers = alloc.alloc(&stream).unwrap();
    println!("Allocated {} buffers", buffers.len());

    // Convert FrameBuffer to MemoryMappedFrameBuffer, which allows reading &[u8]
    let buffers = buffers
        .into_iter()
        .map(|buf| MemoryMappedFrameBuffer::new(buf).unwrap())
        .collect::<Vec<_>>();

    // Create capture requests and attach buffers
    //let mut reqs = buffers
    //    .into_iter()
    //    .map(|buf| {
    //        let mut req = cam.create_request(None).unwrap();
    //        req.add_buffer(&stream, buf).unwrap();
    //        req
    //    })
    //    .collect::<Vec<_>>();
    let reqs = buffers
        .into_iter()
        .enumerate()
        .map(|(i, buf)| {
            let mut req = cam.create_request(Some(i as u64)).unwrap();
            req.add_buffer(&stream, buf).unwrap();
            req
        })
        .collect::<Vec<_>>();

    // Completed capture requests are returned as a callback
    let (tx, rx) = std::sync::mpsc::channel();
    cam.on_request_completed(move |req| {
        tx.send(req).unwrap();
    });

    cam.start(None).unwrap();

    // Enqueue all requests to the camera
    for req in reqs {
        println!("Request queued for execution: {req:#?}");
        cam.queue_request(req).unwrap();
    }

    println!("Done cam, start display");

    // GPIO
    let gpio = Gpio::new().unwrap();
    let dc = gpio.get(SPI_DC).unwrap().into_output();
    let reset = gpio.get(24).unwrap().into_output();
    let mut backlight = gpio.get(BACKLIGHT).unwrap().into_output();

    let button5 = gpio.get(5).unwrap().into_input();
    let button6 = gpio.get(6).unwrap().into_input();

    // SPI Display
    let spi = Spi::new(Bus::Spi0, SlaveSelect::Ss0, 60_000_000_u32, Mode::Mode0).unwrap();
    let di = SPIInterfaceNoCS::new(spi, dc);
    let mut delay = Delay::new();

    let mut display = Builder::st7789(di)
        // width and height are switched on purpose because of the orientation
        .with_display_size(H as u16, W as u16)
        .with_invert_colors(mipidsi::ColorInversion::Inverted)
        .init(&mut delay, Some(reset))
        .unwrap();

    // Text
    let char_w = 10;
    let char_h = 20;
    let text_style = MonoTextStyle::new(&FONT_10X20, Rgb565::RED);
    let text = "Saving";
    let mut text_x = W;
    let mut text_y = H / 2;

    // Alternating color
    let colors = [Rgb565::RED, Rgb565::GREEN, Rgb565::BLUE];

    // Clear the display initially
    display.clear(colors[0]).unwrap();

    // Turn on backlight
    backlight.set_high();
    println!("backlight on");

    display.clear(Rgb565::WHITE).unwrap();

    let mut last = std::time::Instant::now();

    let mut counter = 0;
    loop {
        let mut req = rx
            .recv_timeout(Duration::from_secs(1000))
            .expect("Camera request failed");
        println!("Camera request {:?} completed!", req);

        // Get framebuffer for our stream
        let framebuffer: &MemoryMappedFrameBuffer<FrameBuffer> = req.buffer(&stream).unwrap();
        //println!("FrameBuffer metadata: {:#?}", framebuffer.metadata());

        // MJPEG format has only one data plane containing encoded jpeg data with all the headers
        let planes = framebuffer.data();
        println!("plane count {}", planes.len());
        let jpeg_data = planes.get(0).unwrap().to_owned();
        let data_vec = Vec::from(jpeg_data);

        req.reuse(ReuseFlag::REUSE_BUFFERS);
        cam.queue_request(req).unwrap();

        let elapsed = last.elapsed().as_secs_f64();
        if elapsed < 0.2 {
            continue;
        }

        last = std::time::Instant::now();
        counter += 1;
        if button5.is_low() {
            println!("button 5");
            display.clear(Rgb565::RED).unwrap();
            Command::new("poweroff")
                .output()
                .expect("failed to execute process");
            continue;
        }
        if button6.is_low() {
            println!("button 6");
            display.clear(Rgb565::WHITE).unwrap();
            // Fill the display with alternating colors every 8 frames
            let right = Text::new(text, Point::new(5, 20), text_style)
                .draw(&mut display)
                .unwrap();
            text_x = if right.x <= 0 { W } else { text_x - char_w };

            image::save_buffer(
                format!("/root/images/image{}.png", rng.gen::<u64>()),
                &data_vec,
                800,
                600,
                image::ColorType::Rgb8,
            )
            .unwrap();
            continue;
        }
        let mut transformed = Vec::with_capacity(240 * 240);
        for iii in 0..(240) {
            for jjj in 0..(240) {
                let yyy = (239 - jjj) * 800;
                let xxx = (iii) * 600 / 240;
                let r = data_vec.get((xxx + yyy) * 3).unwrap();
                let g = data_vec.get((xxx + yyy) * 3 + 1).unwrap();
                let b = data_vec.get((xxx + yyy) * 3 + 2).unwrap();
                transformed.push(Rgb565::from(Rgb888::new(
                    r.to_owned(),
                    g.to_owned(),
                    b.to_owned(),
                )))
            }
        }
        display.set_pixels(0, 0, 240, 240, transformed).unwrap();

        if counter > 3000 {
            break;
        }
        println!("it");
    }

    // Turn off backlight and clear the display
    backlight.set_low();
    display.clear(Rgb565::BLACK).unwrap();

    ExitCode::SUCCESS
}
