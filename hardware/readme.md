# Hardware

The case can be 3d printed from the .md3 files, md3 is a common formate for 3d printed projects that contains curves rather than faceted stl files that result in much less smooth prints.

The geometry is saved here rather than gcode so that any printer can have a print sliced using setting suited to that printer.

The original geometry was created in solidworks but its files are not easily opened by other software.

The electrical components required:
* [Pi zero 2](https://www.raspberrypi.com/products/raspberry-pi-zero-2-w/)
* [Pi HQ camera](https://www.raspberrypi.com/products/raspberry-pi-high-quality-camera/)
* Pi lens
* [Tft pi shield](https://www.adafruit.com/product/4506) 
* [Battery shim](https://shop.pimoroni.com/products/lipo-shim)
* Battery
